#include <stdlib.h>
#include <stdio.h>
#include "geometry.h"
#include "list.h"
#include "tree.h"
#include "algo.h"

int main() {
	Segment ho = { { {9,1}, {0,1} }, { {7,1}, {10,1} } };
	Segment yo = { { {2,1}, {3,1} }, { {11,1}, {7,1} } };
	Point a = { {9,1}, {0,1} };
	EventNode * en1 = new_event(a, 0, &ho, &yo);
	EventTree et;
	et.root = en1;
	et.size = 1;
	display_tree_keys(&et, 0);
	//display_segment(ho);
	//display_segment(yo);
	printf("\n");
	printf("\n");
	//system("pwd");
	allPairs((char*) "./data/input", (char*) "./data/output");
	BentleyOttmmann((char*) "./data/input", (char*) "./data/output2");
	//Rational a, b;
	//a.num=3;
	//a.den=7;
	//b.num=2;
	//b.den=5;
	//display_rational(a);
	//display_rational(b);
	//Rational add = radd(a,b);
	//Rational sub = rsub(a,b);
	//display_rational(add);
	//display_rational(sub);
	//Rational mul = rmul(a,b);
	//display_rational(mul);
	//Rational div = rdiv(a,b);
	//display_rational(div);
    Point *i = NULL;
    
    int res = intersect(ho, yo);
    
    i = getIntersectionPoint(ho,yo);
    printf("verification de l'existence d'un point %d \n",res);

    printf("\n");

    if (res==1)
    {
    	printf("Le point d'intersection est :\n");
    	display_point(*i);
    	printf("\n");
    }


    Rational droite = {2,1};

    //test de la fonction seg_prec
    printf("test de deux segments different\n");

    Segment seg1 = { { {1,1}, {5,1} }, { {4,1}, {2,1} } };

    Segment seg2 = { { {1,1}, {1,1} }, { {4,1}, {4,1} } };

    if (seg_prec(seg1, seg2,droite))
            {
            	printf("le segment  ");
            	display_segment(seg1);
            	printf("   precede   ");
            	display_segment(seg2);
            	printf("\n");
            }


	return EXIT_SUCCESS;
}
