#include <stdlib.h>
#include <stdio.h>
#include "list.h"

/*
 * créer et renvoie un nouveau nœud (LNode)
 */
static LNode * new_node(void *data) {
  LNode * newNode = (LNode *)malloc(sizeof(LNode));
  newNode->data = data;

  return newNode;
}

/*
 * créer un élément de liste (LNode)
 * y ranger la donnée (data)
 * insérer l'élément en tête de la liste *list
 */
void list_prepend(List *list, void *data) {

    if (list->tail == NULL) {
        list->size = 1;
        LNode * newNode = new_node(data);
        newNode->next=NULL;
        newNode->prev=NULL;
        list->head = newNode;
        list->tail = newNode;
    }
    else
    {
        LNode * newNode = new_node(data);
        newNode->next=list->head;
        newNode->prev=NULL;
        list->head->prev=newNode;
        list->head=newNode;
        list->size++;
    }
  // Mettre à jour le pointeur sur le noeud précédent la tête de *list
  // Assigner le pointeur sur le noeud suivant de newNode à l'ancienne tête
  // Mettre à jour le pointeur sur la tête de *list, il doit pointer sur newNode
  // Augmenter la taille de 1
  // Penser à utiliser new_node() pour la création du nouveau noeud
}

/*
 * créer un élément de liste (LNode)
 * y ranger la donnée (data)
 * insérer l'élément en queue de la liste *list
 */
void list_append(List *list, void *data) {

    if (list->tail == NULL) {

        list->size = 1;
        LNode * newNode = new_node(data);
        newNode->next=NULL;
        newNode->prev=NULL;
        list->head = newNode;
        list->tail = newNode;
    }
    else
    {
        LNode * newNode = new_node(data);
        newNode->prev=list->tail;
        newNode->next=NULL;
        list->tail->next=newNode;
        list->tail=newNode;
        list->size++;
    }

	// Mettre à jour le pointeur sur le noeud suivant la queue de *list
	// Assigner le pointeur sur le noeud précedant de newNode à l'ancienne queue
	// Mettre à jour le pointeur sur la queue de *list, il doit pointer sur newNode
	// Augmenter la taille de 1
	// Penser à utiliser new_node() pour la création du nouveau noeud
}

/*
 * créer un élément de liste (LNode)
 * y ranger la donnée (data)
 * insérer l'élément dans la liste *list après l'élément prev
 * ce dernier est supposé appartenir effectivement à la liste
 */
void list_insert_after(List *list, void *data, LNode *curr) {
    LNode * newNode = new_node(data);
    
    
    if (list->size==1) {
        list->tail=newNode;
        curr->next=newNode;
        newNode->prev=curr;
        newNode->next=NULL;
    }
    if (curr==list->tail)
    {
        list->tail=newNode;
        curr->next=newNode;
        newNode->prev=curr;
        newNode->next=NULL;
    }
    else
    {
        newNode->prev = curr;
        newNode->next = curr->next;
        curr->next = newNode;
        newNode->next->prev = newNode;
    }

    list->size++;
    // Assigner le pointeur sur le noeud suivant de newNode au noeud pointé par le suivant de *curr
    // Mettre à jour le poiteur du suivant sur le précédent comme étant newNode
    // Assigner le pointeur sur le noeud précedant de newNode à *curr
    // Mettre à jour le pointeur de *curr sur le suivant comme étant newNode
    // Augmenter la taille de 1
    // Penser à utiliser new_node() pour la création du nouveau noeud
}

/*
 * supprimer le premier élément de la liste *list
 * si celle-ci n'est pas vide
 */
void list_remove_first(List *list) {
    if (list->size==1) {
        LNode *adresse_head;
        adresse_head = list->head; //stock l'adresse du premier élément
        list->head=NULL;
        list->tail=NULL;
        free(adresse_head);//on libere de la memoire le noeud
        list->size=0;
    }

    if (list->size>1){
      LNode *adresse_head;
      adresse_head = list->head; //stock l'adresse du premier élément
      list->head = list->head->next; //head pointe sur le deuxieme element
      list->head->prev = NULL;
      free(adresse_head);
      list->size--;
    }  

}

/*
 * supprimer le dernier élément de la liste *list
 * si celle-ci n'est pas vide
 */
void list_remove_last(List *list) {
    if (list->size==1) {
        LNode *adresse_tail;
        adresse_tail = list->tail; //stock l'adresse du premier élément
        list->head=NULL;
        list->tail=NULL;
        free(adresse_tail);//on libere de la memoire le noeud
        list->size=0;
    }

    if (list->size>1){

        LNode *dern_elem;
        dern_elem = list->tail;
        list->tail = list->tail->prev;
        list->tail->next = NULL;
        free(dern_elem);
        list->size--;
      }
}

/*
 * supprimer l'élément pointé par node de la liste *list
 * l'élément est supposé appartenir effectivement à la liste
 */
void list_remove_node(List *list, LNode *node) {
    
    if (list->size==1)
    {
        list->head=NULL;
        list->tail=NULL;
        free(node);
    }
    else if(node==list->head || node==list->tail)
    {
        if (node==list->head)
        {
            list->head=node->next;
            node->next->prev=NULL;
            free(node);
        }
        else
        {
            list->tail=node->prev;
            node->prev->next=NULL;
            free(node);
        }
    }
    else
    {
        node->prev->next = node->next;
        node->next->prev = node->prev;
        //faire le cas avec une petite liste, ou bien le cas lorsque nous traitons un head ou un tail
        free(node);
    }

    list->size--;
}

/*
 * permute les positions des nœuds curr et curr->next
 * dans la liste list
 */
void list_exchange_curr_next(List *list, LNode *curr) {
  //Sauvegarde des adresses au cas ou
  LNode* current = curr;//save de l'adresse de current
  LNode* next_current = curr->next;//save de l'adresse du suivant de current
  LNode* prev_current = curr->prev;//save de l'adresse du precedent de current
    
    //Pour une liste de deux elements
    if(list->size==2)
    {
        // Le cas ou le noeud a changer est en tete de liste
        if(current==list->head)
        {
            list->head=next_current;
            list->tail=current;
            current->prev=next_current;
            current->next=NULL;
            next_current->prev=NULL;
            next_current->next=current;
        }
        //dans le cas ou curr est en fin de liste nous ne faisons rien puisque qu'il n'a pas de suivant
    }
    else if (current==list->head)
    {
        if (current==list->head) {
            list->head=next_current;
            current->next=next_current->next;
            current->prev=next_current;
            next_current->prev=NULL;
            next_current->next=current;
        }
        //dans le cas ou curr est en fin de liste nous ne faisons rien puisque qu'il n'a pas de suivant
    }
    else
    {
        //le cas ou le suivant du coeud courant serait le dernier noeud
        if (list->tail==next_current) {
            prev_current->next = next_current; //Le précédent de current pointe sur le suivant de current qui sera son suivant
            next_current->prev = prev_current; //le suivant de current pointe maintenant sur le precedent de current qui sera son précédent
            current->next = next_current->next; //Current pointe sur le suivant du suivant de current, qui sera son suivant
            current->prev = next_current; //Current pointe sur son suivant qui sera son maintenant son précédent
            next_current->next=current;
            list->tail=current;
        }
        else
        {
            prev_current->next = next_current; //Le précédent de current pointe sur le suivant de current qui sera son suivant
            
            next_current->prev = prev_current; //le suivant de current pointe maintenant sur le precedent de current qui sera son précédent
            
            current->next = next_current->next; //Current pointe sur le suivant du suivant de current, qui sera son suivant
            
            current->prev = next_current; //Current pointe sur son suivant qui sera son maintenant son précédent
            
            next_current->next->prev=current; //
            
            next_current->next=current;
            
        }
    }
}

/*
 * supprimer tous les éléments de la liste *list
 * sans pour autant supprimer leurs données (data)
 * qui sont des pointeurs
 */
void list_destroy(List *list) {
  if (list->size!=0){
    list_remove_first(list);
    list_destroy(list);
  }
  free(list);
}
