#include <stdlib.h>
#include <stdio.h>
#include "algo.h"
#include "tree.h"

/*
 * ranger dans une liste les segments stockés
 * dans le fichier texte de nom infilename
 * dont la première ligne est un entier indiquant
 * le nombre de segments et les lignes suivantes
 * les coordonnées des segments.
 * chaque ligne est composée de 2 couples.
 * Le premier couple est les coordonnées du début du segment
 * au format : xnum / xden ; pour l'abscisse
 * et : ynum / yden ; pour l'ordonnée.
 * Le second couple est la fin du segment
 */
List * load_segments(char *infilename) {
  List *segments = (List*) calloc(1, sizeof(List));

  FILE *fptr;
  if ((fptr = fopen(infilename,"r")) == NULL) {
    printf("Error while opening file %s.", infilename);
    exit(1);
  }
  int size;
  fscanf(fptr, "%d", &size);

  for (int i = 0; i < size; i++) {
    long a1, b1, c1, d1, a2, b2, c2, d2;
    fscanf(fptr, "%ld/%ld,%ld/%ld", &a1, &b1, &c1, &d1);
    fscanf(fptr, "%ld/%ld,%ld/%ld", &a2, &b2, &c2, &d2);
    Segment *s = (Segment*) malloc(sizeof(Segment));
    Point p1 = {{a1,b1},{c1,d1}};
    Point p2 = {{a2,b2},{c2,d2}};
    if (point_prec(p1, p2)) {
      (*s).begin = p1;
      (*s).end = p2;
    }
    else {
      (*s).begin = p2;
      (*s).end = p1;
    }
    list_append(segments, s);
  }
  fclose(fptr);
  return segments;
}

/*
 * ranger dans un fichier texte de nom outfilename
 * les points de la liste *intersections.
 * La premier ligne indique le nombre de points.
 * Puis chaque ligne contient les coordonnée de chaque point.
 * Le format des coordonnées est le même que pour les segments.
 */
void save_intersections(char *outfilename, List *intersections) {
  FILE *fptr;
  if ((fptr = fopen(outfilename,"w")) == NULL) {
    printf("Error while opening file %s.\n", outfilename);
    exit(1);
  }

  fprintf(fptr, "%d\n", intersections->size);
  LNode *curr = intersections->head;
  for (int i = 0; i < intersections->size; i++) {
    fprintf(fptr, "%ld/%ld,%ld/%ld\n",
            ((Point*) curr->data)->x.num,
            ((Point*) curr->data)->x.den,
            ((Point*) curr->data)->y.num,
            ((Point*) curr->data)->y.den);
    curr = curr->next;
  }
  fclose(fptr);
}

/*
 * exécute l'algorithme glouton sur les segments rangés
 * dans le fichier texte de nom infilename et range les points
 * d'intersection dans le fichier texte de nom outfilename
 */
void allPairs(char *infilename, char *outfilename) {
  // A faire
    List* input = (List*) malloc(sizeof(List));
    input->head=NULL;
    input->tail=NULL;
    input->size=0;
    input = load_segments(infilename);
    List* output = (List*) malloc(sizeof(List));
    output->head=NULL;
    output->tail=NULL;
    output->size=0;
    Point *res;
    
    
    
    
    while (input->size>1)
    {
        // creation de pointeur pour sauvegarder l'adresse du premier et dernier element (noeud)
        
        LNode* premier=input->head;
        LNode* dernier=input->tail;
        while (premier!=dernier && !(premier==NULL||dernier==NULL))
        {
            Segment *s1 = ((struct Segment*)premier->data);
            Segment *s2 = ((struct Segment*)dernier->data);
            
            Segment f1 = *s1;
            Segment f2 = *s2;
            if (intersect(f1, f2))
            {
                res = getIntersectionPoint(f1, f2);
                list_append(output, res);
            }
            //Le dernier noeud sera son precedent afin de tester l'intersection avec le premier
            LNode* addresse_avt_dernier = dernier->prev;
            dernier=addresse_avt_dernier;
            //dernier=dernier->prev;
        }
        //enfin on supprime le premier noed vu que nous l'avons tester avec tous les noeuds
        list_remove_first(input);
    }
    
    save_intersections(outfilename, output);

}



//fonction auxiliaure qui permet d'ajouter un segment actif dans une lsa a partir d'un de ses noeud
//le traiment recursif commence a la fin de la liste
// renvoie l'adresse du noeud crée
LNode * inserer_lsa_aux(List *SA, LNode *lsa,EventNode * e)
{
    
    
    Segment * current = (Segment*)lsa->data;
    if(lsa==SA->head)
    {
        //current est segment deja present dans SA
        if (seg_prec(*(e->s1),* current, e->key.x)==1)
        {
            list_insert_after(SA, e->s1, lsa);
            list_exchange_curr_next(SA, lsa);
            return SA->head;
        }
        else
        {
            list_insert_after(SA, e->s1, lsa);
            return lsa->next;
        }
    }
    else
    {
        //Forcement non NULL ce segment
        Segment * seg_prev = (Segment*)lsa->data;
        if (seg_prec(*(e->s1),*current, e->key.x)==1 && seg_prec(*seg_prev,*(e->s1), e->key.x)==1) {
            list_insert_after(SA, e->s1, lsa->prev);
            return lsa->prev;
        }
        else
        {
            return inserer_lsa_aux(SA,lsa->prev, e);
        }
    }
    
    
    
}

//cette procedure ajoute un noeud/Segment de façon à ce qu'elle soit trié selon l'orde de pritorité
// et retourne l'adresse du noeud nouvellement ajouter
LNode* inserer_lsa(List * lsa, EventNode * e)
{
  if(lsa->size==0)
  {
    //insere en tete de liste
    list_prepend(lsa,e->s1);
    return lsa->head;
  }
  else if(lsa->size==1)
  {
    //current est segment deja present dans lsa
    Segment * current = (Segment*)lsa->head->data;

    //ceci est la droite de blayage
    //Segment droite = { { e->s1->begin.x,{0,1} }  ,  { e->s1->begin.x,{0,1}} };
      
    if(seg_prec(*(e->s1),*current,e->key.x )==1)
    {
        list_prepend(lsa, e->s1);
        return lsa->head;
    }
    else
    {
        list_append(lsa, e->s1);
        return lsa->tail;
    }
  }

    // lorsque sa->size >1)
  else
  {
    return inserer_lsa_aux(lsa,lsa->tail,e);
  }
}
//return 1 si les deux segment sont egaux
int eq_seg(Segment * a,Segment * b)
{
  if(eq(a->begin.x,b->begin.x)==1 && eq(a->end.y,b->end.y))
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

//Cette fonction renvoie le successeur d'un segment present dans une liste a partir d'un LNode
//Convention, le parametre LNode doit forcement etre une tete de liste
//Renvoie NULL s'il n y a aucun successeur
LNode * Successeur(List * lsa, LNode * node,Segment * s)
{
    if (lsa->size==0) {
        return NULL;
    }
    if (node==NULL) {
        return NULL;
    }
  Segment * seg = (Segment*)node->data;
  if (node==lsa->tail)
  {
      return NULL;
  }
  if (eq_seg(seg,s)==1)
  {
    return node->next;
  }
  else
  {
    return Successeur(lsa,node->next,s);
  }
}

//Cette fonction renvoie le successeur d'un segment present dans une liste a partir d'un LNode
//Convention, le parametre LNode doit forcement etre une fin de liste
//Renvoie NULL s'il n y a aucun predecesseur
LNode * Predecesseur(List * lsa, LNode * node,Segment * s)
{
    if (lsa->size==0) {
        return NULL;
    }
    if (node==NULL) {
        return NULL;
    }
  Segment * seg = (Segment*)node->data;
  if (node==lsa->head)
  {
      return NULL;
  }
  if (eq_seg(seg,s)==1)
  {
    return node->prev;
  }
  else
  {
    return Predecesseur(lsa,node->prev,s);
  }
}

//fonction aux qui permet l'extraction a partir d'un LNode de segment
Segment* extract_lsa_aux(List * lsa,LNode *la,EventNode * e)
{
  if(la==lsa->tail)
  {
    if(eq_seg(e->s1,(Segment*)la->data)==1)
    {
     list_remove_node(lsa, la);
     return e->s1;
    }
    else
    {
      return NULL;
    }
  }
  else if(eq_seg(e->s1,(Segment*)la->data)==1)
  {
    list_remove_node(lsa, la);
    return e->s1;
  }
  else
  {
    return extract_lsa_aux(lsa,la->next,e);
  }
}
//cette fonction extrait un segment d'une liste
Segment* extract_lsa(List *lsa,EventNode * e)
{
  if(lsa->size==0)
  {
    return NULL;
  }
  else
  {
    return extract_lsa_aux(lsa,lsa->head,e);
  }
}

//permute deux segments dans une liste
// si une des deux segments n'y est pas, la fonction ne fait rien
void permutation(List * LSA,Segment * s1,Segment * s2)
{
    LNode* pt_s1 = (LNode*) malloc(sizeof(LNode));
    pt_s1 = NULL;
    LNode* pt_s2 = (LNode*) malloc(sizeof(LNode));
    pt_s2 = NULL;
    
    LNode * tete = LSA->head;
    while(tete != LSA->tail)
    {
        Segment * temp =(Segment*)tete->data;
        if (eq_seg(temp,s1))
        {
            pt_s1 = tete;
        }
        if (eq_seg(temp,s2))
        {
            pt_s2 = tete;
        }
        tete=tete->next;
    }
    if (pt_s2!=NULL && pt_s1!= NULL) {
        //sauvegarde du pointeur sur les donnée de s1
        void *data1 = pt_s1->data;
        pt_s1->data=pt_s2->data;
        pt_s2->data=data1;
    }
}

//Ceci est la procedure Traiter-Debut-Segment
void deb_seg(EventNode * e,List * lsa,EventTree * a,List * li)
{

  //Segment s = e->key;
  //Segment * pt_s = &s;

  //j'ai un doute ici. Sur le fait que pt_s a bien pris la valeur ou pas
  Segment* pt_s = (Segment*) malloc(sizeof(Segment));
  
  pt_s=e->s1;

  LNode * pt_noeud_seg = inserer_lsa(lsa,e);

  if(pt_noeud_seg->next!=NULL)
  {
    Segment *ps_s2 = (Segment*)pt_noeud_seg->next->data;
    Point *i1 =NULL;
    if (intersect(*pt_s,*ps_s2)==1)
    {
      i1 = getIntersectionPoint(*pt_s,*ps_s2);
    }
        if (i1!=NULL)
    {
      list_prepend(li,i1);
      insert_event(a,new_event(*i1,0,pt_s,ps_s2));
    }
  }
  else if(pt_noeud_seg->prev!=NULL)
  {
    Segment *ps_s3 = (Segment*)pt_noeud_seg->prev->data;
    Point *i2 =NULL;
    if (intersect(*pt_s,*ps_s3)==1)
    {
      i2 = getIntersectionPoint(*pt_s,*ps_s3);
    }
    if (i2!=NULL)
    {
      list_prepend(li,i2);
      insert_event(a,new_event(*i2,0,pt_s,ps_s3));
    }
  }
}

void end_seg(EventNode * e,List * lsa,EventTree * a,List * li)
{
  //le successeur du segment e
  LNode * success = Successeur(lsa,lsa->head,e->s1);
  LNode * predecc = Predecesseur(lsa,lsa->tail,e->s1);

  //extraction du segment de lsa
  Segment * s = extract_lsa(lsa,e);
  s=NULL;

  if(success != NULL && predecc!=NULL )
  {
    Point* i = (Point*) malloc(sizeof(Point));

    Segment * util1 = (Segment*)success->data;
    Segment * util2 = (Segment*)predecc->data;
      
    if (intersect(*util2,*util1)==1)
    {
      i = getIntersectionPoint(*util2,*util1);
      list_prepend(li,i);
      insert_event(a,new_event(*i,0,(Segment*)success->data,(Segment*)predecc->data));
    }
  }

}

void inter_seg(EventNode * e,List * lsa,EventTree * a,List * li)
{
  LNode * success_s2 = Successeur(lsa,lsa->head,e->s2);
  LNode * predecc_s1 = Predecesseur(lsa,lsa->tail,e->s1);

  Segment * seg1 = e->s1;
  Segment * seg2 = e->s2;


  if (predecc_s1 != NULL)
  {
    Point* i1 = (Point*) malloc(sizeof(Point));
    Segment * seg_predec = (Segment*)predecc_s1->data;
      if (intersect(*seg2,*seg_predec)==1)
      {
          i1 = getIntersectionPoint(*seg2,*seg_predec);
          list_prepend(li,i1);
      }
  }
  else if (success_s2 != NULL)
  {
    Point* i2 = (Point*) malloc(sizeof(Point));
    Segment * seg_success = (Segment*)success_s2->data;
      if (intersect(*seg1,*seg_success)==1) {
          i2 = getIntersectionPoint(*seg1,*seg_success);
          list_prepend(li,i2);
      }
  }

    permutation(lsa,seg1,seg2);

}
/*
 * exécute l'algorithme Bentley-Ottmmann sur les segments rangés
 * dans le fichier texte de nom infilename et range les points
 * d'intersection dans le fichier texte de nom outfilename
 */
void BentleyOttmmann(char *infilename, char *outfilename) {

  //Chargement et initialisation de la liste des segments
  List* input = (List*) malloc(sizeof(List));
  input->head=NULL;
  input->tail=NULL;
  input->size=0;
  input = load_segments(infilename);

  //initialisation de l'arbre de priorité des evenement
  //doit contenir tous les extremites ddes segments
  List* input_temp = (List*) malloc(sizeof(List));
  input_temp->head=NULL;
  input_temp->tail=NULL;
  input_temp->size=0;
  input_temp = load_segments(infilename);


  EventTree * a = (EventTree *)malloc(sizeof(EventTree));
  a->root=NULL;
  a->size=0;

  while(input_temp->size >=1)
  {
    
    Segment *seg = ((struct Segment*)input_temp->head->data);
    
    //EventNode * na= new_event(seg->begin,1,seg,NULL);
    //EventNode * nb= new_event(seg->end,2,seg,NULL);
    insert_event(a,new_event(seg->begin,1,seg,NULL));
    insert_event(a,new_event(seg->end,2,seg,NULL));
    list_remove_first(input_temp);
  }

  //Initialisation de la liste de segments actifs
  List* lsa = (List*) malloc(sizeof(List));
  lsa->head=NULL;
  lsa->tail=NULL;
  lsa->size=0;

  //initialisation de la liste des points d'intersection
  List* output = (List*) malloc(sizeof(List));
  output->head=NULL;
  output->tail=NULL;
  output->size=0;

  while(a->size != 0)
  {
    EventNode * s = get_next_event(a);
    if(s->type==2)
    {
      end_seg(s,lsa,a,output);
    }
    else if(s->type==1)
    {
      deb_seg(s,lsa,a,output);
    }
    else
    {
      inter_seg(s,lsa,a,output);
    }
  }
    save_intersections(outfilename, output);


}
